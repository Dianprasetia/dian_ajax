-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 25, 2020 at 05:19 AM
-- Server version: 5.7.26-0ubuntu0.18.04.1-log
-- PHP Version: 7.3.6-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `latihan_ci`
--

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE `guru` (
  `nik` int(30) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `jenis_kelamin` varchar(25) NOT NULL,
  `telp` varchar(30) NOT NULL,
  `alamat` text NOT NULL,
  `id` int(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`nik`, `nama`, `jenis_kelamin`, `telp`, `alamat`, `id`) VALUES
(1352425345, 'Bu Riyani Koswara', 'Perempuan', '08654256531264', 'Badakarya Punggelan', 2),
(13131313, 'Bu Siti Aisyah', 'Perempuan', '0876765653', 'Bawang Banjarnegara', 3),
(61173541, 'Bu Refiana Saputri', 'Perempuan', '0825426285428', 'Bawang Banjarnegara mn', 4),
(56848284, 'Bu Putri Nur Aliyah', 'Perempuan', '0866436242', 'Wanadadi,banjarnegara', 6);

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `nik` int(20) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `jenis_kelamin` varchar(35) NOT NULL,
  `telp` varchar(30) NOT NULL,
  `alamat` text NOT NULL,
  `id` int(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`nik`, `nama`, `jenis_kelamin`, `telp`, `alamat`, `id`) VALUES
(2147483647, 'Bu Riyani Koswara', 'Perempuan', '0852443373554', 'Banjarnegaraaaaaa', 14),
(12221231, 'Bu Siti Aisyah', 'Perempuan', '0856425534768', 'Banjarnegaara', 16),
(443525243, 'Bu Sabrina ', 'Perempuan', '085242552988', 'Banjarnegaraaaa', 17),
(1263526, 'Bu Refiana Saputri', 'Perempuan', '085675426383', 'Banjarmangu', 18),
(163648, 'Bu Riyani Koswara', 'Perempuan', '0857632715', 'Badakarya', 19),
(163648, 'Bu Riyani Koswara', 'Perempuan', '0857632715', 'Badakarya', 20),
(11, 'hgasj', 'Laki-laki', '9786', 'djaks', 21),
(11, 'sah', 'Laki-laki', '127', 'haddghk', 22),
(11, 'dadja', 'Laki-laki', '2716535', 'ggadashhhs', 23),
(111, 'dsasna', 'Laki-laki', '62545', 'fgjeg', 24);

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE `pengguna` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `umur` int(3) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_kelamin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`id`, `nama`, `umur`, `tanggal_lahir`, `jenis_kelamin`) VALUES
(3, 'Riyani Koswara', 1, '2221-11-11', 0),
(4, 'Riyani Koswara', 1, '2221-11-11', 0),
(5, 'Riyani Koswara', 1, '2221-11-11', 0),
(6, 'Riyani Koswara', 1, '2221-11-11', 0),
(7, 'Riyani Koswara', 1, '2221-11-11', 0),
(8, 'Riyani Koswara', 1, '2221-11-11', 0),
(9, 'Riyani Koswara', 1, '2221-11-11', 0),
(10, 'Riyani Koswara', 1, '2221-11-11', 0),
(11, 'Riyani Koswara', 1, '2221-11-11', 0),
(12, 'Riyani Koswara', 1, '2221-11-11', 0),
(13, 'Riyani Koswara', 1, '2221-11-11', 0),
(14, 'Riyani Koswara', 1, '2221-11-11', 0),
(15, 'Riyani Koswara', 1, '2221-11-11', 0),
(16, 'Riyani Koswara', 1, '2221-11-11', 0),
(17, 'Riyani Koswara', 1, '2221-11-11', 0),
(18, 'Riyani Koswara', 1, '2221-11-11', 0),
(19, 'Riyani Koswara', 1, '2221-11-11', 0),
(20, 'Riyani Koswara', 1, '2221-11-11', 0),
(21, 'Riyani Koswara', 2, '2003-12-23', 0),
(22, 'ia', 12, '2003-12-05', 0),
(23, 'ya', 17, '2003-12-31', 0),
(24, 'ya', 17, '2003-12-31', 0),
(25, 'ya', 17, '2003-12-31', 0),
(26, 'ya', 17, '2003-12-31', 0),
(27, 'q', 12, '1111-11-12', 0),
(28, 'ff', 16, '2003-02-23', 0),
(29, 'dian', 27, '0666-06-06', 0),
(30, 'dian', 27, '0666-06-06', 0),
(31, 'DianPrasetia', 12, '0002-11-22', 0),
(32, 'DianPrasetia', 12, '0002-11-22', 0),
(33, 'ddd', 11, '2020-03-10', 0),
(34, 'ddd', 11, '2020-03-10', 0),
(35, 'siti', 12, '2020-03-11', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_mapel`
--

CREATE TABLE `tb_mapel` (
  `kodepelajaran` varchar(12) NOT NULL,
  `matapelajaran` varchar(12) NOT NULL,
  `namapengarang` varchar(25) NOT NULL,
  `kelas` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_mapel`
--

INSERT INTO `tb_mapel` (`kodepelajaran`, `matapelajaran`, `namapengarang`, `kelas`) VALUES
('JAHEJKQ', 'SFK', 'JRKS', 'DDJK');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pelajaran`
--

CREATE TABLE `tb_pelajaran` (
  `kodepelajaran` varchar(30) NOT NULL,
  `matapelajaran` varchar(25) NOT NULL,
  `namapengarang` varchar(20) NOT NULL,
  `kelas` int(14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pelajaran`
--

INSERT INTO `tb_pelajaran` (`kodepelajaran`, `matapelajaran`, `namapengarang`, `kelas`) VALUES
('62547254', 'Bahasa Indonesia', 'Erlangga', 11),
('676356', 'Matematika', 'Erlangga', 11),
('84683', 'Bahasa Indonesia', 'Erlangga', 12),
('664264', 'Bahasa Inggris', 'Erlangga', 10),
('765436', 'Sejarah Indonesia', 'Erlangga', 10);

-- --------------------------------------------------------

--
-- Table structure for table `tb_siswa`
--

CREATE TABLE `tb_siswa` (
  `noinduk` varchar(6) DEFAULT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `hobi` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_siswa`
--

INSERT INTO `tb_siswa` (`noinduk`, `nama`, `alamat`, `hobi`) VALUES
('521642', 'Riyani Koswara', 'Badakarya Punggelan,banjarnegara', 'basket'),
('765566', 'Siti Aisyah', 'Bawang,banjarnegara  jawa tengah', 'volly'),
('665216', 'Sabrina ', 'Banjarmangu jawa tengah', 'tari'),
('43435', 'refianasaputri', 'bawang', 'renang');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_mapel`
--
ALTER TABLE `tb_mapel`
  ADD PRIMARY KEY (`kodepelajaran`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `guru`
--
ALTER TABLE `guru`
  MODIFY `id` int(13) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id` int(13) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `pengguna`
--
ALTER TABLE `pengguna`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
