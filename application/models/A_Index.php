<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class A_Index extends CI_Model {

 function TampilSiswa(){
 	
 	return $this->db->from('tb_siswa');
}
	function getData(){ 
		return $this->db->get('tb_pelajaran')->result();
	}

	function getDataByKodepelajaran($kodepelajaran){
		$this->db->where('kodepelajaran',$kodepelajaran);
		return $this->db->get('tb_pelajaran')->result();
	}

	function deleteData($kodepelajaran){
		$this->db->where('kodepelajaran',$kodepelajaran);
		$this->db->delete('tb_pelajaran');
	}

	function insertData($data){
		$this->db->insert('tb_pelajaran',$data);
	}

function updateData($kodepelajaran,$data){
		$this->db->where('kodepelajaran',$kodepelajaran);
		$this->db->update('tb_pelajaran',$data);
	}
}