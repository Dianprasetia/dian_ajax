<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class D_Index extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('A_Index');
	}

	public function index()
	{ 
		$data['hasil']=$this->A_Index->TampilSiswa();
		$this->load->view('B_Index');
	}

	function ambilData(){
		$data = $this->A_Index->getData();
		echo json_encode($data);
	}

	function ambilDataByKodepelajaran(){
		$kodepelajaran= $this->input->post('kodepelajaran');
		$data = $this->A_Index->getDataByKodepelajaran($kodepelajaran);
		echo json_encode($data);
	}

	function hapusData(){
		$kodepelajaran= $this->input->post('kodepelajaran');
		$data = $this->A_Index->deleteData($kodepelajaran);
		echo json_encode($data);
	}

	function tambahData(){
		$kodepelajaran = $this->input->post('kodepelajaran');
		$matapelajaran 	= $this->input->post('matapelajaran');
		$namapengarang	= $this->input->post('namapengarang');
		$kelas		   = $this->input->post('kelas');

		$data = ['kodepelajaran' => $kodepelajaran, 'matapelajaran' => $matapelajaran, 'namapengarang' => $namapengarang, 'kelas' => $kelas];
		$data = $this->A_Index->insertData($data);
		echo json_encode($data);
	}

	function perbaruiData(){
		$kodepelajaran = $this->input->post('kodepelajaran');
		$matapelajaran  = $this->input->post('matapelajaran');
		$namapengarang 	= $this->input->post('namapengarang');
		$kelas 		    = $this->input->post('kelas');

		$data = ['kodepelajaran' => $kodepelajaran, 'matapelajaran' => $matapelajaran, 'namapengarang' => $namapengarang, 'kelas' => $kelas];

		$data = $this->A_Index->updateData($kodepelajaran, $data);
		
		echo json_encode($data);
	}
}