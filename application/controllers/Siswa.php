 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('model_siswa');
	}

	public function index()
	{
		$data['hasil']=$this->model_siswa->TampilSiswa();
		$this->load->view('V_Siswa',$data);
	}
}
